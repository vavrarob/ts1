package cz.cvut.fel.ts1;

public class Vavrarob {
    public long factorialIterative(int n) throws Exception {
        if (n < 0) throw new Exception("Argument je zaporny!");
        long result = 1;
        while(n > 0){
            result *= n;
            n--;
        }
        return result;
    }

    public long factorialRecursive(int n) {
        if(n < 0) new RuntimeException("Argument je zaporny!");
        if (n <= 1) return 1;
        return n * factorialRecursive(n - 1);
    }
    public long factorialRecursiveNew(int n) {
        if(n < 0) new RuntimeException("Argument je zaporny!");
        if (n <= 1) return 1;
        return n * factorialRecursive(n - 1);
    }
    public static void main(String[] args) throws Exception {
        Vavrarob myClass = new Vavrarob();
        System.out.println(myClass.factorialIterative(3));
    }
}

