package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class VavrarobTest {
    @Test
    public void factorialIterativeTest() throws Exception {
        Vavrarob myClass = new Vavrarob();
        long result = myClass.factorialIterative(4);
        long expectedResult = 24;
        assertEquals(expectedResult, result);
    }

    @Test
    public void factorialIterativeNewTest() throws Exception {
        Vavrarob myClass = new Vavrarob();
        long result = myClass.factorialRecursiveNew(4);
        long expectedResult = 24;
        assertEquals(expectedResult, result);
    }

    @Test
    public void factorialRecursiveTest() throws Exception {
        Vavrarob myClass = new Vavrarob();
        long result = myClass.factorialRecursive(4);
        long expectedResult = 24;
        assertEquals(expectedResult, result);
    }

    @Test
    public void factorialRecursiveThrowsExceptionTest() {
        Vavrarob myClass = new Vavrarob();
        assertThrows(Exception.class, () -> myClass.factorialIterative(-1));
    }

}
